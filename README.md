
# TF-Bitbucket-Runner

Running the apply step of the most recently run pipeline on the master branch of this 
repo will spin up a self-hosted bitbucket runner for utilization. 
The destroy step will completely tear down the runner. 

To use the self-hosted runner in a pipeline, add a "runs-on" node to the bitbucket-pipelines.yml file like this:


```
pipelines:
  custom:
    pipeline:
      - step:
          name: Step1
          size: 8x # default 4gb, 8x for 32gb
          runs-on: 
            - 'self.hosted'
            - 'linux'
          script:
            - echo "This step will run on a self hosted runner with 32 GB of memory.";
      - step:
          name: Step2
          script:
            - echo "This step will run on Atlassian's infrastructure as usual.";
```

More info about configuring your pipeline yml file for a self-hosted runner can be found here:

https://support.atlassian.com/bitbucket-cloud/docs/configure-your-runner-in-bitbucket-pipelines-yml/


TODO: refine this terraform business to better handle multiple branches/multiple runners etc.
