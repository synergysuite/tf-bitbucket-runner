variable "cloudflare" {
    type = map(string)
    default = {
        domain = "synergysuite.com"
        zone_id = "37d52708b593e2ee8db6c4a91aa46a18"
    }
}

variable "awsprops" {
    type = map(string)
    default = {
        region = "eu-west-1"
        vpc = "vpc-e8100c8a"
        ami = "ami-0940ef4494702509f"
        itype = "m4.xlarge"
        subnet = "subnet-846e4dc2"
        publicip = true
        keyname = "ubuntu-instance-keypair"
        secgroupname = "bb-runner-sg"    #### need to make this dynamic for future templatized use probably based on repo slug
    }
}

variable "awstags" {
    type = map(string)
    default = {
        name = "Bitbucket Runner"   
        Environment = "production"
        feature = "CICD"
    }
}

variable "BITBUCKET_REPO_SLUG" {
    description = "bitbucket repo name pulled from env variables in pipeline config"
    type = string
}

variable "CLOUDFLARE_EMAIL" {
    description = "cloudflare email pulled from env variables in pipeline config"
    type = string
}

variable "CLOUDFLARE_API_TOKEN" {
    description = "cloudflare token pulled from env variables in pipeline config"
    type = string
}