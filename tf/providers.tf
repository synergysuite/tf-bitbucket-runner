terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
    region = lookup(var.awsprops, "region")
}

provider "cloudflare" {
    email = var.CLOUDFLARE_EMAIL
    api_token = var.CLOUDFLARE_API_TOKEN
}