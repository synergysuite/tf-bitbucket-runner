resource "aws_instance" "bb_runner" {
  ami = lookup(var.awsprops, "ami")
  instance_type = lookup(var.awsprops, "itype")
  subnet_id = lookup(var.awsprops, "subnet") #FFXsubnet2
  associate_public_ip_address = lookup(var.awsprops, "publicip")
  key_name = lookup(var.awsprops, "keyname")


  vpc_security_group_ids = [
    aws_security_group.bb_runner_sg.id
  ]
  root_block_device {
    delete_on_termination = true
    volume_size = 50
    volume_type = "gp2"
  }
  tags = {
    Name = lookup(var.awstags, "name")
    Environment = lookup(var.awstags, "Environment")
    feature = lookup(var.awstags, "feature")
  }

  depends_on = [ aws_security_group.bb_runner_sg ]

  user_data = "${file("user-data.sh")}"
}

resource "aws_security_group" "bb_runner_sg" {
  name = lookup(var.awsprops, "secgroupname")
  description = lookup(var.awsprops, "secgroupname")
  vpc_id = lookup(var.awsprops, "vpc")

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "cloudflare_record" "bb_runner" {
    zone_id = lookup(var.cloudflare, "zone_id")
    name    = var.BITBUCKET_REPO_SLUG
    value   = aws_instance.bb_runner.public_dns
    type    = "CNAME"
    proxied = false
}