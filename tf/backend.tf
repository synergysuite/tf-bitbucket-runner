terraform {
  backend "s3" {
    bucket = "synergysuite-tf-backend"
    encrypt = true
    region = "eu-west-1"
  }
}