# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.12.2"
  constraints = "~> 3.0"
  hashes = [
    "h1:8Hj+fxNa4EnoKYYSrQGWmK3wLKipJ01Wweue9J+lIsE=",
    "zh:0e78da2bd698cd164718b69a86de43f5842ca3e7d62be54706fd0ff25ca5da5c",
    "zh:45deb5312de674f1b90e6ffb8a3e10c544b852095a0ed71f523e2ec6443c7e60",
    "zh:4b2bf4411da63950cc413f83a0e4612c1e731d06360326d5e9697c74dd5cbeb6",
    "zh:5bfc629bf65447c1a293a4410ca9b61c87526b084b81b2cb2f8bc605f81c2da6",
    "zh:6fc8d57f24e4ceaa3a0a0f43bef2601410f957cefd4d7a8194abd89bcb431eec",
    "zh:75ef23b472e30189d8a2d41a1b01fbf94ec9e5d90cc1a52753f5123ad763d99a",
    "zh:7fbe2753519ba870a48a43e7939d70591633b2a32ff576405d50964d8a28755e",
    "zh:9979599b8f1a7be2e591ae83afdd5249761b5487e7b3cce0ea629ca9350d7e90",
    "zh:b1ec9a7adfdce85858d5b9495470732d8e162a4444f0ac74b5aa55179bfa632d",
    "zh:b597923a78bf12e4c77e003c5396c13385ef2cff645bc6904eb7ccccd76f819c",
    "zh:b904893b6db9f9b0f2fc81053500689d5fee7c43caf032ab830e0418d8339c59",
    "zh:bb674c6d6da7f5f44208854ac318fab40f4edf4ffd3c49be457b505c14e004b1",
    "zh:bdd4fbf9274eb249c539295cff4da82727d682d91c1c5d7ee3ee3490ebefad79",
    "zh:c5b7d7ca0bb5236c8a3fccad4a10cfce03c06eee243d1b0b281df22aa73bb4c8",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.10.0"
  hashes = [
    "h1:S6xGPRL08YEuBdemiYZyIBf/YwM4OCvzVuaiuU6kLjc=",
    "zh:0a2a7eabfeb7dbb17b7f82aff3fa2ba51e836c15e5be4f5468ea44bd1299b48d",
    "zh:23409c7205d13d2d68b5528e1c49e0a0455d99bbfec61eb0201142beffaa81f7",
    "zh:3adad2245d97816f3919778b52c58fb2de130938a3e9081358bfbb72ec478d9a",
    "zh:5bf100aba6332f24b1ffeae7536d5d489bb907bf774a06b95f2183089eaf1a1a",
    "zh:63c3a24c0c229a1d3390e6ea2454ba4d8ace9b94e086bee1dbdcf665ae969e15",
    "zh:6b76f5ffd920f0a750da3a4ff1d00eab18d9cd3731b009aae3df4135613bad4d",
    "zh:8cd6b1e6b51e8e9bbe2944bb169f113d20d1d72d07ccd1b7b83f40b3c958233e",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:c5c31f58fb5bd6aebc6c662a4693640ec763cb3399cce0b592101cf24ece1625",
    "zh:cc485410be43d6ad95d81b9e54cc4d2117aadf9bf5941165a9df26565d9cce42",
    "zh:cebb89c74b6a3dc6780824b1d1e2a8d16a51e75679e14ad0b830d9f7da1a3a67",
    "zh:e7dc427189cb491e1f96e295101964415cbf8630395ee51e396d2a811f365237",
  ]
}
