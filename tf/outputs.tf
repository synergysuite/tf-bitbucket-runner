# output "url" {
#  value = "https://${cloudflare_record.bb_runner.name}.${lookup(var.cloudflare, "domain")}"
# }

output "ec2instance" {
  value = aws_instance.bb_runner.public_ip
}

output "dnsName" {
  value = cloudflare_record.bb_runner.hostname
}